# Documents for RTOS & Train Control System in CS452 at the University of Waterloo

1. Kernel.pdf describes the data structures, routines, and details about the real-time microkernel
2. TC1.pdf describes the structure of routing server and the algorithms we applied on routing
3. TC2.pdf describes the structure of collision detection server and how do we prevent and recover collision
4. TC3.pdf describes the final project - the chasing game. It discusses the manual of this game and some fancy features
